const router = require('express').Router();
const asyncMiddleware = require('../middlewares/async.middleware');
const auth = require('../middlewares/auth.middleware');
const {
  createProduct,
  getProducts,
  getAllProducts,
  getProduct,
  updateProduct,
} = require('../controllers/product.controller');

router.get('/', auth, asyncMiddleware(getProducts));
router.get('/all', auth, asyncMiddleware(getAllProducts));
router.get('/:id', auth, asyncMiddleware(getProduct));
router.post('/', auth, asyncMiddleware(createProduct));
router.patch('/:id', auth, asyncMiddleware(updateProduct));

module.exports = router;
