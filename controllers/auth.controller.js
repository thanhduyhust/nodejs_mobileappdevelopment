const service = require('../services/auth.service');

async function register(req, res) {
  const user = await service.insert(req.body);
  res.status(200).send({ user });
}

async function login(req, res) {
  const token = await service.login(req.body);
  res.status(200).send({ token });
}

async function update(req, res) {
  const { firstName, lastName } = req.body;
  const user = await service.update(req.userId, firstName, lastName);
  res.status(200).send({ user });
}

async function changePassword(req, res) {
  const { password, newPassword } = req.body;
  await service.changePassword(req.userId, password, newPassword);
  res.status(200).send({
    success: {
      message: 'Change password successfully',
    },
  });
}

async function getInfo(req, res) {
  const user = await service.get(req.userId);
  res.status(200).send({
    user,
  });
}

module.exports = { register, login, update, changePassword, getInfo };
