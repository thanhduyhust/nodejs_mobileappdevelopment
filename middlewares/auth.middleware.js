const jwt = require('jsonwebtoken');
const authService = require('../services/auth.service');

async function auth(req, res, next) {
  try {
    const token = req.header('Authorization');
    const decode = jwt.verify(token, process.env.JWT_SECRET || 'abcxyz');
    const userId = parseInt(decode.id, 10);
    const user = await authService.findByIdAndToken(userId, token);

    if (!user) {
      res.status(401).send({
        error: {
          message: 'Unauthorized',
        },
      });
    }

    req.token = token;
    req.userId = userId;
    next();
  } catch (e) {
    res.status(401).send({
      error: {
        message: 'Unauthorized',
      },
    });
  }
}

module.exports = auth;
