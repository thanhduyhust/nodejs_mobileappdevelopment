const getErrorDefaults = require('../errors/errorDefaults');
const CustomError = require('../errors/CustomError');

// eslint-disable-next-line no-unused-vars
function handleCommonError(err, req, res, next) {
  // eslint-disable-next-line prefer-const
  let { code, message } = getErrorDefaults(err.code);
  message = err.sqlMessage || err.message || message;
  if (err.sqlMessage) {
    code = 422;
  }
  if (err.statusCode) {
    code = err.statusCode;
  }
  if (err.status) {
    code = err.status;
  }

  res.status(code).send({
    error: {
      message,
    },
  });
}

function handle405Error(req, res, next) {
  if (!req.route) {
    throw new CustomError(405, `${req.method} method not allowed`);
  }
  let flag = false;
  req.route.stack.forEach(stack => {
    if (req.method === stack.method) {
      flag = true;
    }
  });
  // for (let i = 0; i < req.route.stack.length; i++) {
  //   if (req.method === req.route.stack[i].method) {
  //     flag = true;
  //   }
  // }
  if (!flag) {
    // err = new Error('Method Not Allowed');
    // err.status = 405;
    // return next(err);
    throw new Error(405, `${req.method} not allowed`);
  }

  next();
}

module.exports = { handleCommonError, handle405Error };
