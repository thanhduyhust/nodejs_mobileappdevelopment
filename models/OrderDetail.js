const { Model } = require('objection');

class OrderDetail extends Model {
  static get tableName() {
    return 'orderDetails';
  }

  static get idColumn() {
    return 'id';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [
        'title',
        'price',
        'description',
        'imageUrl',
        'quantity',
        'orderId',
      ],
      properties: {
        id: { type: 'integer' },
        title: { type: 'string' },
        price: { type: 'number' },
        description: { type: 'string' },
        imageUrl: { type: 'string' },
        quantity: { type: 'number' },
        orderId: { type: 'integer' },
      },
    };
  }
}

module.exports = OrderDetail;
