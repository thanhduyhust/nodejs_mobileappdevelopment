const Order = require('../models/Order');
const OrderDetail = require('../models/OrderDetail');
const Product = require('../models/Product');
const { now } = require('../utils/sqlTimestamp');

async function getList(userId) {
  const orders = [];

  const dbOrders = await Order.query()
    .select('orders.*')
    .where('ownerId', userId);

  await Promise.all(
    dbOrders.map(async (dbOrder) => {
      const dbOrderDetails = await OrderDetail.query()
        .select('orderDetails.*')
        .where('orderId', dbOrder.id);

      const order = dbOrder;
      order.cartItems = dbOrderDetails;

      orders.push({
        order,
      });
    }),
  );

  return orders;
}

async function insert(userId, cartItems) {
  const orderDetails = [];

  await Promise.all(
    cartItems.map(async (cartItem) => {
      const product = await Product.query().findById(cartItem.productId);
      const orderDetail = {
        title: product.title,
        description: product.description,
        price: product.price,
        imageUrl: product.imageUrl,
        quantity: cartItem.quantity,
      };

      orderDetails.push(orderDetail);
    }),
  );

  const totalAmount = orderDetails.reduce((current, detail) => {
    return detail.price * detail.quantity + current;
  }, 0);

  const dbOrder = await Order.query().insert({
    date: now(),
    totalAmount,
    ownerId: userId,
  });

  await Promise.all(
    orderDetails.map(async (orderDetail) => {
      const detail = orderDetail;
      detail.orderId = dbOrder.id;
      await OrderDetail.query().insert(detail);
      return detail;
    }),
  );

  const order = dbOrder;
  order.cartItems = orderDetails;
  return order;
}

module.exports = { getList, insert };
