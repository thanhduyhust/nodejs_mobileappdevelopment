const jwt = require('jsonwebtoken');
const { bcrypt, bcryptCompare } = require('../utils/encryption');
const User = require('../models/User');
const CustomError = require('../errors/CustomError');

async function generateAuthToken(id) {
  const token = jwt.sign(
    { id: id.toString() },
    process.env.JWT_SECRET || 'abcxyz',
    {
      expiresIn: '30 days',
    },
  ); // return String

  await User.query().findById(id).patch({
    token,
  });

  return token;
}

async function checkExistedEmail(email) {
  const users = await User.query().select('users.*').where('email', email);

  if (users.length > 0) {
    throw new CustomError(400, 'Email is existed');
  }
}

async function insert(userInfo) {
  if (typeof userInfo.password !== 'string') {
    throw new CustomError(422, 'Password must be string');
  }

  if (userInfo.password.length < 6) {
    throw new CustomError(
      405,
      'Password must not be shorter than 6 characters',
    );
  }

  await checkExistedEmail(userInfo.email);

  const user = await User.query().insert({
    email: userInfo.email,
    password: await bcrypt(userInfo.password),
    name: userInfo.name,
    phone: userInfo.phone,
  });

  user.token = await generateAuthToken(user.id);
  user.password = undefined;

  return user;
}

async function login(userInfo) {
  const users = await User.query()
    .select('users.*')
    .where('email', userInfo.email);

  const user = users[0];
  if (!user || !(await bcryptCompare(userInfo.password, user.password))) {
    throw new CustomError(401);
  }

  const token = await generateAuthToken(user.id);

  return token;
}

async function findByIdAndToken(id, token) {
  if (!(id && token)) {
    throw new CustomError(401);
  }

  const users = await User.query()
    .select('email', 'password', 'name', 'phone')
    .where('id', id)
    .where('token', token);

  if (users.length === 0) {
    throw new CustomError(401);
  }

  return users[0];
}

// async function update(id, firstName, lastName) {
//   // if (!firstName && !lastName)
//   await User.query().findById(id).patch(
//     snakeCaseKeys({
//       lastName,
//       firstName,
//     }),
//   );

//   const user = await User.query().findById(id);

//   if (!user) {
//     throw new CustomError(405, 'Update successfully but something was wrong');
//   }

//   user.password = undefined;
//   user.token = undefined;

//   return user;
// }

async function changePassword(id, password, newPassword) {
  if (!(password && newPassword)) {
    throw new CustomError(400, 'Current password & new password are required ');
  }

  if (newPassword.length < 6) {
    throw new CustomError(
      400,
      'Password must not be shorter than 6 characters',
    );
  }

  if (!bcryptCompare(password, await User.query().findById(id).password)) {
    throw new CustomError(400, 'Current password was wrong');
  }

  await User.query()
    .findById(id)
    .patch({
      password: await bcrypt(newPassword),
    });
}

async function get(userId) {
  const user = await User.query().findById(userId);
  user.password = undefined;
  user.token = undefined;

  return user;
}

// update(1, { lastName: 'Truong', firstName: 'Thanh Duy' });

module.exports = {
  insert,
  login,
  findByIdAndToken,
  // update,
  changePassword,
  get,
};
